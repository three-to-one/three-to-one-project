// Import of moment.js
import moment from 'moment';

// After importing moment.js. Import of current timezone. 
import 'moment/locale/fr';

// Connect timeZone with moment.js.
moment.locale('fr');

// Import of ccs stylesheet, to compile and to be in webpack.
import "./css/style.css";

// Declare variable x=O, to show the diffrence beetwen months, 
// it was set to 0 bcuz there is no difference beetwen the current month and the month displayed in the calendar acctually.
let x = 0;

// Decale of an object, where will be saved all of the events wich will be gerenated or created. 
// Every new event will be saved in this object.
const dataSave = {};

// Here will be stocked the exact day we select.
let selectDate = "";

// Here will be stocked the current month (format :number), that will be used after, to refind the current month and the difference
// beetween the month we are actually at.
// Me percaktu sa muje ka ndermjet, me ane te X-it.
// Afishon muajin kurent gjithashtu, nuk e di pse i duhet -1.
const currentMonth = moment().format('M') - 1;

// Declare changeNameMonth function to be executed when we need it, or call, with the buttons, to go to the next or previous month.
// And eventually we call the function in the main page to see the current month.
// First we select the h1 we set in the index.html with #name and stock/remplace next valeur. We set it equal to the current month
// with moment.js plus X. in the long format.
function changeNameMonth() {
    document.getElementById('name').innerHTML = moment().month(currentMonth + x).format('MMMM - YYYY');
    generateNumberMonth();
}

//
function generateNumberMonth() {

    // This variable is equal the number of days in a month. daysInMonth() method belong to moment.js
    const daysInMonth = moment().month(currentMonth + x).daysInMonth();

    // This variable conteins the name of the days, that after will be used with the number of month included.
    let tableHTML = " <thead class='thead-dark'>" +
        "<tr>" +
        "<th scope='col'>Lu</th>" +
        "<th scope='col'>Ma</th>" +
        "<th scope='col'>Me</th>" +
        "<th scope='col'>Je</th>" +
        "<th scope='col'>Ve</th>" +
        "<th scope='col'>Sa</th>" +
        "<th scope='col'>Di</th>" +
        "</tr>" +
        "</thead>" +
        "<tbody>";


    // This const gives us the first day of the (current month or difference), it depends wich month is selected.
    // (example: 2019-02-01)
    const begin = moment().month(currentMonth + x).format('YYYY-MM-') + '01';

    // This const gives us the DAY in string. // Diten e pare te muajit qe e ke seleksionu me difference.
    // (example: vendredi) for the first day of Fevrier 
    const day = moment(begin).format("dddd");

    // This const contains un object with the name of days.
    const days = { "lundi": 0, "mardi": 1, "mercredi": 2, "jeudi": 3, "vendredi": 4, "samedi": 5, "dimanche": 6 }

    // This const contains the difference, when the month starts
    // (example: 4) is the difference , for when to start the numbers. it depend witch nameDay starts the month.
    const diff = days[day];

    // This Loop will be repeated. and look after number of days in month(daysInMonth) + diff wich is the difference where to start the number.
    // (example: daysInMonth = 28 + diff= 4 ) it gives 32 in total for the month, or 28 without the difference.
    // The difference is represented as empty numbers.
    
    for (let i = 0; i < (daysInMonth + diff); i++) {
        // Sa here perserietet loop e krijon nje date me format (shembull 2019-02-01 etc)
        const date = moment().month(currentMonth + x).format('YYYY-MM-') + (i + 1 - diff);
        // This changes the format of date.
        const verif = moment(date).format('DD  MMMM  YYYY');
        // Pas cdo numri qe plotpjestohet me 7 dhe mbetja eshte 0 dhe numri i nuk eshte 0, sepse 0 plotepjestohet me 7/
        // At'here mbyllet linja dhe kalohet ne linje tjeter.
        if (i % 7 === 0 && i !== 0) {
            tableHTML += "</tr>";
        }

        // Kjo e hap linjen.
        if (i % 7 === 0) {
            tableHTML += "<tr>";
        }

        // Hapesirat e thata, dmth diferenca.
        if (i < diff) {
            tableHTML += `<td></td>`;
        // Ne rast qe kemi dicka te ruajtur ne ate date, kjo e verifikon dhe e shton klasen event.
        }else if(dataSave[verif]) {
            tableHTML += `<td class='data event'>${i + 1 - diff}</td>`;
        // Data pa event po qe duhet te afishohet.
        } else {
            tableHTML += `<td class='data'>${i + 1 - diff}</td>`;
        }
    }

    // Close the tbody, the bottom of the table.
    tableHTML += `</tbody>`;

    // We add the table we just did into the table of html with the id=calendar. In order to see what we did.
    document.getElementById('calendar').innerHTML = tableHTML;

    // changeEventName(moment()); // to be deleted.

    // To every balise html with the class=data add an eventListener in click. 
    
    document.querySelectorAll('.data').forEach(data => data.addEventListener('click', function () {
    // Remove the class = cfaredush, it means, remove the color/background when clicked. Remove for all clicked.
        document.querySelectorAll('.cfaredush').forEach(cfaredush => cfaredush.classList.remove('cfaredush'));
    // Add class= cfareduush, represented with a light greenblue color in this code, when a day is clicked.
        this.classList.add('cfaredush');
    // The number into clicked date.(numri 9 psh, brenda hapsires qe klikojme).
        const date = moment().month(currentMonth + x).format('YYYY-MM-') + this.textContent;
    // Call the function changeEventName into this function bcuz we need to change the date in every click, so the clicked date
    // and the displayed date just next to the button <Ajouter un evenement> will corespond.
    // afer butonit ne eventit, sa here klikojme na afishohet data qe e kemi kliku n'kalendar, kte na mundeson thirrja e keti
    // funksioni ketu.
        changeEventName(date);
    }));
}

// He takes in parametre the date we want to display.
function changeEventName(date) {
// We add selectDate, in order to know when we save a new event which day we selected.
    selectDate = moment(date).format('DD  MMMM  YYYY');
// We add to our paragraph selected date. (daten qe eshte e seleksionuar).
    document.getElementsByClassName('para')[0].innerHTML = selectDate.toUpperCase();
    // list-of-events, e krijon listen e eventeve, edhe i afishon kur i kemi kriju.
    let listHTML = "";
    if(dataSave[selectDate]) {
        dataSave[selectDate].forEach(event => {
            listHTML +=`<li class='list-group-item'> ${event.sujet} - ${event.notes}</li>`;
        })
    }
    // We put the list into html.
    document.getElementById('list-of-events').innerHTML = listHTML;
}
// We call this function to display the actual month, before doing that, he calculates wich month we are at.
changeNameMonth();

// We select the button previous month and add an event in click, that will calculete for the previus month and disply it, once the 
// button is clicked.
document.getElementById('btn1').addEventListener('click', function () {
    x--;
    changeNameMonth();
})

// We select the button Next month and add an event in click, that will calculete for the next month(or month to comme) and disply it, 
// once the button is clicked. 
document.getElementById('btn2').addEventListener('click', function () {
    x++;
    changeNameMonth();
})

// We select the button <enregistrer> and add an EventListener function in click.
document.getElementById('btn-send').addEventListener('click', function (e) {
// We prevent to refresh the page by default.
    e.preventDefault();
// We get the value of the first input called Sujet.
    const sujet = document.getElementById('sujet').value;
// We get the value of the second input called Notes.
    const notes = document.getElementById('notes').value;
// If the selected date has not a table, add a table.
    if(!dataSave[selectDate]) {
        dataSave[selectDate] = [];
    }
// Into the table of the selected date add/push the value of the inputs.
    dataSave[selectDate].push({sujet, notes});
// We stock the element with the className 'cfaredush into a variable.
    const divQfareDush = document.getElementsByClassName('cfaredush')[0];
// We add to the variable we just created the classList 'event', to know that we have an event in a random day.
    divQfareDush.classList.add('event');

// The event we create..
    const date = moment().month(currentMonth + x).format('YYYY-MM-') + divQfareDush.textContent;
// ...we send to the function changeEventName to display the event. (just below the button<ajouter un evenement>)
    changeEventName(date);
// We set the form in hide. In order not to be displayed all the time but when clicked the button with correspondin 
// event. and reset to empty the value of Sujet and Notes.   
    $(".modal").modal("hide")
    document.getElementById('sujet').value = "";
    document.getElementById('notes').value = "";
})
