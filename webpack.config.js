
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
module.exports = {
  entry: './src/index.js',
  // mode:"development",
  output: {
    path: path.join(__dirname, 'dist'),
    // publicPath: '/dist', // instead of publicPath: '/build/' 
    filename: '[name].js'
},
  module: {
      rules: [{
          test: /\.css$/,
          use: [
              "style-loader", // creates style nodes from JS strings
              "css-loader", // translates CSS into CommonJS
              //"sass-loader" // compiles Sass to CSS, using Node Sass by default
          ]
      }]
  },plugins: [
    new HtmlWebpackPlugin({template: './src/index.html'}),
    new MomentLocalesPlugin({
      localesToKeep: ['fr', 'fr'],
  }),
    
  ],
  devtool:'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
};
